package adapters;


import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import sensorSantander.R;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import datos.Alarma;
import datos.AlarmaRegistrada;
import utilities.CardViewManage;
import utilities.Interfaces_MVP;

public class ListAlarmasAdapter extends BaseAdapter {

    private final ArrayList<Alarma> listaAlarmas;
    private final LayoutInflater inflater;
    private final Interfaces_MVP.ViewFavoritosYAlarma mView;

    public ListAlarmasAdapter(List<Alarma> listaAlarmas, Interfaces_MVP.ViewFavoritosYAlarma view){
        inflater = LayoutInflater.from(view.getActivityContext());
        this.listaAlarmas = (ArrayList<Alarma>) listaAlarmas;
        mView = view;
    }

    @Override
    public int getCount() {
        return listaAlarmas.size();
    }

    @Override
    public Object getItem(int position) {
        return listaAlarmas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View vistaCardAlarmas = inflater.inflate(R.layout.card_alarmas, parent, false);


        final Alarma alarma = listaAlarmas.get(position);


        TextView tvAlarmName = vistaCardAlarmas.findViewById(R.id.tv_alarm_name);
        TextView tvAlarmMedidaLabel = vistaCardAlarmas.findViewById(R.id.tv_alarm_medidalabel);
        TextView tvAlarmMedida = vistaCardAlarmas.findViewById(R.id.tv_alarm_medida);
        ImageButton borrarAlarma = vistaCardAlarmas.findViewById(R.id.ib_alarm_borrar);

        tvAlarmName.setText(alarma.getNombre());
        tvAlarmMedida.setText(String.valueOf(alarma.getValorAlarma()));

        String tipo = alarma.getTipoAlarma();
        String maxMin = alarma.getMaxMin();
        switch (tipo) {
            case "temp":
                if (maxMin.equals("max")) {
                    tvAlarmMedidaLabel.setText("Temp. max: ");
                } else {
                    tvAlarmMedidaLabel.setText("Temp. min: ");
                }
                break;
            case "luz":
                if (maxMin.equals("max")) {
                    tvAlarmMedidaLabel.setText("Luz max: ");
                } else {
                    tvAlarmMedidaLabel.setText("Luz. min: ");
                }
                break;
            case "ruido":
                if (maxMin.equals("max")) {
                    tvAlarmMedidaLabel.setText("Ruido max: ");
                } else {
                    tvAlarmMedidaLabel.setText("Ruido. min: ");
                }
                break;
        }

        borrarAlarma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listaAlarmas.remove(alarma);
                notifyDataSetChanged();
                mView.updateListAlarmas(listaAlarmas);
            }
        });

        //Parte desplegable de la card view
        final LinearLayout hiddenView;
        hiddenView = vistaCardAlarmas.findViewById(R.id.hidden_alarmas_view);


        vistaCardAlarmas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hiddenView.getVisibility() == View.VISIBLE) {
                    CardViewManage.collapse(hiddenView);
                }else if(hiddenView.getVisibility() == View.GONE){
                    CardViewManage.expand(hiddenView);
                }
            }
        });

        //Todas las alarmas registradas del sensor
        ArrayList<AlarmaRegistrada> alarmasRegistradas = alarma.getAlarmasRegistradas();

        //Borramos las de 2 dias antes
        alarmasRegistradas.removeAll(listarAlarmasRegistradas(alarmasRegistradas));

        mView.updateListAlarmasRegistradas(alarma, alarmasRegistradas);

        //Listar cada alarma registrada
        hiddenView.removeAllViews();
        for(AlarmaRegistrada al : alarmasRegistradas){
            View line = View.inflate(mView.getActivityContext(), R.layout.lista_alarmas_activadas, null);
            TextView tvName = line.findViewById(R.id.alarma_activada_nombre);
            String textAlarma = "<b>Valor: </b>" + al.getValor().toString() + "  <b>Fecha:</b> " + al.getFecha();
            tvName.setText(Html.fromHtml(textAlarma));
            tvName.setPaddingRelative(32,0,0,0);
            hiddenView.addView(line);
        }

        return vistaCardAlarmas;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private ArrayList<AlarmaRegistrada> listarAlarmasRegistradas(ArrayList<AlarmaRegistrada> alarmasRegistradas){

        ArrayList<AlarmaRegistrada> alarmasRegBorradas = new ArrayList<>();

        LocalDate fechaActual = LocalDate.now();
        fechaActual = fechaActual.minusDays(1);

        for (Iterator<AlarmaRegistrada> iterator = alarmasRegistradas.iterator(); iterator.hasNext();) {
            AlarmaRegistrada al = iterator.next();

            LocalDate fechaSensor = LocalDate.parse(al.getFechaReal());

            if(fechaSensor.isBefore(fechaActual)){
                iterator.remove();
                alarmasRegBorradas.add(al);
            }
        }
        return alarmasRegBorradas;
    }
}
