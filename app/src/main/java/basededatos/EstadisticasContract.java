package basededatos;

import android.provider.BaseColumns;

public class EstadisticasContract {

    private EstadisticasContract(){
        throw new IllegalStateException("Utility class");
    }

    public abstract static class MedidasSensorEntry implements BaseColumns {

        private MedidasSensorEntry(){
            throw new IllegalStateException("Utility class");
        }

        public static final String TABLE_NAME ="medidas";

        public static final String ID = "id";
        public static final String ID_SENSOR = "id_sensor";
        public static final String FECHA = "fecha";
        public static final String FECHACORTADA = "fechaCortada";
        public static final String TEMP = "temp";
        public static final String RUIDO = "ruido";
        public static final String LUZ = "luz";
    }

}
