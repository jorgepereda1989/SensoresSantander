package datos;

import java.util.ArrayList;

public class VariablesGlobales {

    private VariablesGlobales(){
        throw new IllegalStateException("Utility class");
    }

    private static ArrayList<String> nombreGrupos = new ArrayList<>();

    public static ArrayList<String> getNombreGrupos() {
        return nombreGrupos;
    }

    public static void setNombreGrupos(ArrayList<String> nombreGrupos) {
        VariablesGlobales.nombreGrupos = nombreGrupos;
    }


}
