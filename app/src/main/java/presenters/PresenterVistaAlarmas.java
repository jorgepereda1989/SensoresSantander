package presenters;

import android.content.Intent;
import android.view.MenuItem;

import sensorSantander.R;
import sensorSantander.VistaFavoritos;


import utilities.Interfaces_MVP;


public class PresenterVistaAlarmas implements Interfaces_MVP.PresenterAlarma {

    // View reference.
    private Interfaces_MVP.ViewFavoritosYAlarma mView;


    public PresenterVistaAlarmas(Interfaces_MVP.ViewFavoritosYAlarma view){
        mView = view;
    }


    @Override
    public boolean menuAlarmas(MenuItem item) {

        if (item.getItemId() == R.id.action_home_alarma) {
            Intent goFavs = new Intent(mView.getActivityContext(), VistaFavoritos.class);
            goFavs.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mView.getActivityContext().startActivity(goFavs);
            return true;
        }
        return false;
    }
}