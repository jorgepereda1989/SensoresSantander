package presenters;

import android.content.Intent;
import android.view.MenuItem;

import sensorSantander.R;
import sensorSantander.VistaFavoritos;
import com.google.android.gms.maps.GoogleMap;

import java.util.ArrayList;

import datos.SensorAmbiental;
import utilities.Interfaces_MVP;
import utilities.TipoMapa;


public class PresenterVistaMapa implements Interfaces_MVP.PresenterMapa {

    // View reference.
    private Interfaces_MVP.ViewMapa mView;

    private ArrayList<SensorAmbiental> sensorAmbList;

    public PresenterVistaMapa(Interfaces_MVP.ViewMapa view, ArrayList<SensorAmbiental> sensores){
        mView = view;
        sensorAmbList = sensores;
    }


    @Override
    public void showServerNotAvailable() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void showConnectionNotAvailable() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean menuMapa(MenuItem item, GoogleMap map) {
        TipoMapa tipo = new TipoMapa(sensorAmbList);

        switch (item.getItemId()) {
            case R.id.filtro_fecha:
                mView.dialogFiltrarFechas();
                return true;
            case R.id.action_home:
                Intent goFavs = new Intent(mView.getActivityContext(), VistaFavoritos.class);
                mView.getActivityContext().startActivity(goFavs);
                return true;
            case R.id.todos:
                //todos los sensores
                tipo.mapaCompleto(map);
                return true;
            case R.id.weather:
                //sensores atmosfericos
                tipo.mapaWeather(map);
                return true;
            case R.id.noise:
                //sensores de ruido
                tipo.mapaRuido(map);
                return true;
            default:
                return menuMapa(item, map);
        }
    }
}
