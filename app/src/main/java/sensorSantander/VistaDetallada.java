package sensorSantander;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;



import java.util.ArrayList;

import datos.Alarma;
import datos.AlarmaRegistrada;
import datos.Parent;
import datos.SensorAmbiental;
import datos.VariablesGlobales;
import presenters.PresenterVistaFavoritos;
import utilities.Interfaces_MVP;

public class VistaDetallada extends AppCompatActivity implements AdapterView.OnItemSelectedListener, Interfaces_MVP.ViewFavoritosYAlarma {

    private static Interfaces_MVP.PresenterFavoritos mPresenter;

    private SensorAmbiental sensor;

    private String id;
    private String ruido;
    private String luminosidad;
    private String temperatura;
    private String ultMod;

    private String grupoSeleccionado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vista_detalle);
        mPresenter = new PresenterVistaFavoritos(this);

        ArrayList<String> grupos = VariablesGlobales.getNombreGrupos();

        Intent intent = getIntent();
        sensor = (SensorAmbiental) intent.getSerializableExtra("sensor");

        id = sensor.getIdentificador();
        ruido = sensor.getRuido();
        luminosidad = sensor.getLuminosidad();
        temperatura = sensor.getTemperatura();
        ultMod = sensor.getUltModificacion();

        llenarCampos();

        Spinner selectorGrupo = findViewById(R.id.selector_grupo);
        selectorGrupo.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, grupos));
        selectorGrupo.setOnItemSelectedListener(this);

        Button botonAddFav = findViewById(R.id.boton_add_favoritos);
        botonAddFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.onClickAddFavorito(sensor, grupoSeleccionado);
            }
        });

    }



    public void llenarCampos(){
        TextView idTV = findViewById(R.id.id_detalle_text);
        TextView tempTV = findViewById(R.id.temp_detalle_text);
        TextView noiseTV = findViewById(R.id.noise_detalle_text);
        TextView lightTV = findViewById(R.id.light_detalle_text);
        TextView ultModTV = findViewById(R.id.ult_modificacion_text);

        idTV.setText(id);
        tempTV.setText(temperatura);
        noiseTV.setText(ruido);
        lightTV.setText(luminosidad);
        String year = ultMod.substring(0,4);
        String mes = ultMod.substring(5,7);
        String dia = ultMod.substring(8,10);
        String hora = ultMod.substring(11,16);
        ultModTV.setText(dia+"/"+mes+"/"+year+" "+hora);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.selector_grupo) {
            grupoSeleccionado = parent.getSelectedItem().toString();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void addToGroup(Parent grupo) {
        throw new UnsupportedOperationException();
    }

    @Override
    public PresenterVistaFavoritos getPresenter() {
        return (PresenterVistaFavoritos) mPresenter;
    }

    @Override
    public void updateListTotal(ArrayList<SensorAmbiental> sensorAmbList) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateListView(ArrayList<Parent> parents) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void actionModeEditar(int groupPosition) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void runService(Service service, String intentString, ArrayList intentList) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void stopServicioStats() {
        throw new UnsupportedOperationException();
    }

    @Override
    public ExpandableListView getExpList() {
        return null;
    }


    @Override
    public void refreshScreen() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateParentInList(Parent parent) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateListParents(ArrayList<Parent> parents) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateListAlarmas(ArrayList<Alarma> alarmas) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateListAlarmasRegistradas(Alarma alarma, ArrayList<AlarmaRegistrada> alarmasRegistradas) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void updateAlarmInList(Alarma alarma) {
        throw new UnsupportedOperationException();
    }
}
