package sensorSantanderTest;

import junit.framework.TestCase;

import org.junit.Before;

import datos.Parent;
import datos.SensorAmbiental;

public class ParentTest extends TestCase {

    private Parent mParent;

    @Before
    public void setUp(){
        mParent = new Parent("parent");
    }

    public void testAddChild() {
        assertTrue(mParent.addChild(createSensor("Sensor1")));
    }

    public void testRemoveChild() {
        SensorAmbiental sensor = createSensor("testSensor");
        mParent.addChild(sensor);

        assertTrue(mParent.removeChild(sensor));
        SensorAmbiental fakeSensor = createSensor("fakeSensor");
        assertFalse(mParent.removeChild(fakeSensor));
    }

    public SensorAmbiental createSensor(String text){
        SensorAmbiental sensor = new SensorAmbiental();
        sensor.setIdentificador("00008");
        sensor.setTitulo(text);
        return sensor;
    }
}