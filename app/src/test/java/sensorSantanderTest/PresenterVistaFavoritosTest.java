package sensorSantanderTest;


import android.view.Menu;
import android.widget.EditText;

import com.example.sensorSantander.R;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import basededatos.Medidas;
import basededatos.MedidasController;
import datos.Parent;
import datos.SensorAmbiental;
import presenters.PresenterVistaFavoritos;
import sensorSantander.VistaFavoritos;
import utilities.Interfaces_MVP;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.RETURNS_DEEP_STUBS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PresenterVistaFavoritosTest {

    private PresenterVistaFavoritos mPresenter;
    private Parent mockParent;
    private SensorAmbiental mockSensor;
    private Medidas mockMedidas;
    private MedidasController mockController;
    private Interfaces_MVP.ViewFavoritosYAlarma mockView;

    @Before
    public void setup() {
        // Creating the mocks
        mockView = Mockito.mock(Interfaces_MVP.ViewFavoritosYAlarma.class);
        mockParent = Mockito.mock( Parent.class, RETURNS_DEEP_STUBS );
        mockMedidas = Mockito.mock( Medidas.class, RETURNS_DEEP_STUBS );
        mockSensor = Mockito.mock( SensorAmbiental.class, RETURNS_DEEP_STUBS);
        mockController  = Mockito.mock( MedidasController.class, RETURNS_DEEP_STUBS );
        // Pass the mocks to a Presenter instance
        mPresenter = new PresenterVistaFavoritos( mockView );
        //mPresenter.setModel(mockModel);
        // Define the value to be returned by Model
        // when loading data
        //when(mockModel.loadData()).thenReturn(true);
        reset(mockView);
    }

    @Test
    public void testOnClickAddFavorito() {
        // We need to mock a EditText
        EditText mockEditText = Mockito.mock(EditText.class, RETURNS_DEEP_STUBS);
        // the mock should return a String
        when(mockEditText.getText().toString()).thenReturn("Test_true");
        when(mockParent.addChild(any(SensorAmbiental.class))).thenReturn(true);
        mPresenter.onClickAddFavorito(mockSensor,"grupo");
        verify(mockParent).addChild(any(SensorAmbiental.class));
    }

    @Test
    public void testonClickAddRecogidaMedidas() {
        mPresenter.onClickAddRecogidaMedidas(mockSensor, 1);
        when(mockController.nuevaMedida(mockMedidas)).thenReturn((long) 1);
    }

    @Test
    public void testgetListaSensores() {
        
    }

    @Test
    public void testMenuFavoritos() {
        VistaFavoritos mockVista = Mockito.mock( VistaFavoritos.class, RETURNS_DEEP_STUBS );
        Menu optionsMenu = null;
        assertTrue(mPresenter.menuFavoritos(optionsMenu.getItem(R.id.action_refresh_list), mockVista));
        assertTrue(mPresenter.menuFavoritos(optionsMenu.getItem(R.id.irMapa), mockVista));
        assertTrue(mPresenter.menuFavoritos(optionsMenu.getItem(R.id.verAlarmas), mockVista));
        assertTrue(mPresenter.menuFavoritos(optionsMenu.getItem(R.id.action_add_element), mockVista));
    }
}
